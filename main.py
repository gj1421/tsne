import numpy as np
import matplotlib.pyplot as plt
from matplotlib import offsetbox
from sklearn.manifold import TSNE
import os
import cv2
from sklearn import (manifold, datasets, decomposition, ensemble,
                     discriminant_analysis, random_projection)


# scale and move the coordinates so they fit [0; 1] range
def scale_to_01_range(x):
    # compute the distribution range
    value_range = (np.max(x) - np.min(x))

    # move the distribution so that it starts from zero
    # by extracting the minimal value from all its values
    starts_from_zero = x - np.min(x)

    # make the distribution fit [0; 1] by dividing by its range
    return starts_from_zero / value_range


def replace_backslash(path):
    new_path = path.replace(os.sep, '/')
    return new_path


def iterate_dict(my_dict):
    for key in my_dict:
        print(key)
        for value in my_dict[key]:
            print(value)


my_dict = {}
directory = r'subsub'
out_ = []


def create_dict():
    for folderName in os.listdir(directory):
        # print(folderName)
        list_pictures = list()
        file_path = replace_backslash(os.path.join(directory, folderName))
        for fileName in os.listdir(file_path):
            if fileName.endswith(".jpg") or fileName.endswith(".png"):
                image_path = replace_backslash(os.path.join(directory, folderName, fileName))
                immatrix = cv2.imread(image_path, cv2.COLOR_BGR2RGB)
                immatrix = np.array(cv2.imread(image_path, cv2.IMREAD_GRAYSCALE).flatten())
                # immatrix = immatrix.transpose(2, 0, 1).reshape(3, -1)
                # csvFilePath = os.path.join('data', fileName + '.csv')
                # csvFilePath = replace_backslash(csvFilePath)
                # np.savetxt(csvFilePath, immatrix)

                # print(immatrix)
                # save to csv file
                list_pictures.append(np.array(immatrix))
                out_.append(np.array(immatrix))
        my_dict[folderName] = list_pictures
    return my_dict


# len(list_pictures) = 28
# len(list_pictures[0]) = 920

def all_in_one_array(_dict):
    my_list = list()
    for key in _dict:
        for value in _dict[key]:
            my_list.extend(value)
    return my_list


def all_in_one_dictionary(_dict):
    my_list = list()
    for key in _dict:
        for value in _dict[key]:
            my_list.append(value)
    return my_list


create_dict()
print(my_dict['Cubism'])
print((my_dict['Cubism']))
# allInOne = all_in_one_dictionary(my_dict)

# # # X = np.array([[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 1]])
# X = allInOne
# X = np.vstack(allInOne)
# print(X.shape)
# X_embedded = TSNE(n_components=2).fit_transform(X)
# print(X_embedded.shape)
# print(X_embedded)
# plt.scatter(X_embedded[:, 0], X_embedded[:, 1])
# plt.xlabel('x-tsne')
# plt.ylabel('y-tsne')
# plt.title('t-SNE')
# plt.show()
