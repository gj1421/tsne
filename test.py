import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import offsetbox
from sklearn.manifold import TSNE
import os
import cv2

# sns.set(rc={'figure.figsize': (11.7, 8.27)})
palette = sns.color_palette('coolwarm', n_colors=1)
x, y = np.loadtxt('data.csv', unpack=True, usecols=(0, 1))
data = np.loadtxt('data.csv', unpack=True, usecols=(0, 1))

# print(x)
# print(y)
# plt.scatter(x, y)
# plt.xlabel('x-tsne')
# plt.ylabel('y-tsne')
# plt.title('t-SNE')
# plt.show()

X = np.array(data)
# print(X)
X_embedded = TSNE(n_components=3).fit_transform(X)
print(X_embedded.shape)
sns.scatterplot(X_embedded[:, 0], X_embedded[:, 1], hue=X_embedded[:, 2], legend='full')

# plt.scatter(X_embedded[:, 0], X_embedded[:, 1])
# plt.xlabel('x-tsne')
# plt.ylabel('y-tsne')
# plt.title('t-SNE')
plt.show()
